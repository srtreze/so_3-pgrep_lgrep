# -*- coding: utf-8 -*-

#####################################################################
#########################----- Imports -----#########################
#####################################################################


import sys, os.path, pickle as p


#######################################################################
#########################----- Variables -----#########################
#######################################################################


file_name = ""


########################################################################
#########################----- executable -----#########################
########################################################################

if len(sys.argv) == 1:
    print
    print "  Falta o nome do ficheiro que pretende ler"
    print

else:

    file_name = sys.argv[1]

    if os.path.isfile(file_name):

        binario = open(file_name, "rb")

        file_contents = []

        while True:

            try:

               file_contents.append(p.load(binario))

            except EOFError:
                break

        start = file_contents.pop(-2)
        demorou = file_contents.pop(-1)

        conteudos_do_bin = {}

        while len(file_contents) > 0:

            pid = file_contents.pop(0)

            conteudos_do_bin[pid] = {}

            conteudos_do_pid = file_contents.pop(0)

            for file_detail in conteudos_do_pid:
                conteudos_do_bin[pid][file_detail[0]] = file_detail[1:]

        binario.close()

        print
        print "  Início da execução da pesquisa: " + start
        print
        print "  Duração da execução: " + demorou

        for pid in conteudos_do_bin:

            print
            print "  Processo:", pid

            for fic in conteudos_do_bin[pid]:
                print
                print "\t  ficheiro:", fic
                print "\t\t  tempo de pesquisa:", conteudos_do_bin[pid][fic][0]
                print "\t\t  dimensão do ficheiro:", conteudos_do_bin[pid][fic][1]
                print "\t\t  número de ocorrências:", conteudos_do_bin[pid][fic][2]

        print

    else:
        print
        print "  O ficheiro", file_name, "nao esxiste"
        print