# -*- coding: utf-8 -*-

#####################################################################
#########################----- Imports -----#########################
#####################################################################


from multiprocessing import Process, Semaphore, Value

import sys, os, os.path, ctypes, datetime, signal, pickle


#######################################################################
#########################----- Variables -----#########################
#######################################################################

# variaveis globais ###################################################

args = sys.argv[1:]

palavra = ""
fics=[]
fic_pos = Value("i", 0)

busca_fic = Semaphore(1)
vou_incrementar = Semaphore(1)
vou_fazer_print = Semaphore(1)

num_linhas = Value("i", 0)

# variaveis para serem usadas no o ficheiro binario ###################

nome_fic = ""

tempo_execucao = 0
#conteudo_por_fic = manager.dict()

# variaveis para processos ############################################

num_processes = 0

# variaveis de intervalo de tempo #####################################

start_time = 0
intervalo_tempo = 0
fics_vistos = Semaphore(1)
num_fics_vistos = Value("i",0)
vou_escrever = Semaphore(1)

#######################################################################
#########################----- Functions -----#########################
#######################################################################


def pesquisa_fic(escrever):

    global fics
    true = True
    x= int(os.getpid())

    os_meus_fics = []

    vou_incrementar.acquire()
    #conteudo_por_fic[x] = {}
    vou_incrementar.release()

    while true:

        busca_fic.acquire()
        if fic_pos.value < len(fics):
            fich = fics[fic_pos.value]
            fic_pos.value += 1
            busca_fic.release()

            linhas_por_fic = 0

            if os.path.isfile(fich):
                tamanho_do_fic = os.path.getsize(fich)
                start_fic = datetime.datetime.now()
                fic = open(fich, 'r')
                linhas = []
                for linha in fic:
                    if palavra.value in linha:

                        linhas_por_fic += 1

                        linhas.append(fich + ": " + linha.lstrip().rstrip())
                fic.close()

                tempo_fic = datetime.datetime.now() - start_fic

                vou_fazer_print.acquire()
                num_linhas.value += linhas_por_fic
                print ""
                for linha in linhas:
                    print linha
                vou_fazer_print.release()

                fics_vistos.acquire()
                num_fics_vistos.value += 1
                fics_vistos.release()

                os_meus_fics.append([fich, str(tempo_fic), tamanho_do_fic, linhas_por_fic])

            else:
                print
                print fich, "nao e um ficheiro."

        else:
            true = False
            busca_fic.release()

    if escrever == 1:

        vou_escrever.acquire()

        binario = open(nome_fic.value, "ab")

        pickle.dump(x, binario, 2)
        pickle.dump(os_meus_fics, binario, 2)

        binario.close()

        vou_escrever.release()


def recebe_fic():

    global fics

    print "  Insira o(s) ficheiro(s) que pretende analisar"
    print "  Quando tiver incerido todos os ficheiros, insira '.exit.' numa nova linha"

    while True:
        fic_input = str(raw_input())
        if fic_input == ".exit.":
            return
        else:
            fic = fic_input.split()
            for f in fic:
                fics.append(f)


def imprime_estado(sig, NULL):

    global tempo_execucao
    global intervalo_tempo
    global start_time

    tempo_execucao = datetime.datetime.now() - start_time

    print
    print "       Estado:"
    print
    print "  Numero de linhas:", num_linhas.value
    print "  Numero de ficheiros vistos:", num_fics_vistos.value
    print "  Tempo de execucao:", int(tempo_execucao.total_seconds() * 1000000)


def ctrl_c(sig, NULL):
    busca_fic.acquire()
    fic_pos.value = len(fics)
    busca_fic.release()


########################################################################
#########################----- executable -----#########################
########################################################################


if len(args) == 0:
    print
    print "  Erro: falta de argumentos"
    print "  Em caso de duvida, digite a opcao 'man'."
    print
    sys.exit()

else:

    signal.signal(signal.SIGINT, ctrl_c)

############################################################## caso -p #

    if args[0] == "-p":
        if len(args) >= 3:

            try:
                num_processes = int(float(args[1]))
            except ValueError:
                print
                print "  Numero de processos invalido."
                print "  Em caso de duvida, digite a opcao 'man'."
                print
                sys.exit()

        ################################################### caso -p -a #

            if args[2] == "-a":

                if len(args) >= 5:

                    try:
                        intervalo_tempo = float(args[3])
                    except ValueError:
                        print
                        print "  Intervalo de tempo invalido."
                        print "  Em caso de duvida, digite a opcao 'man'."
                        print
                        sys.exit()

                ############################################ caso -p -a -f #

                    if args[4] == "-f":

                        if len(args) >= 7:

                            nome_fic = Value(ctypes.c_char_p, args[5])

                            if os.path.isfile(nome_fic.value):
                                print
                                print "  O ficheiro " + nome_fic.value + " já existe."
                                print
                                sys.exit()
                            
                            ##############################################################
                            ### FALTA try/except (o nome_fic não pode começar com "/") ###
                            ##############################################################

                            palavra = Value(ctypes.c_char_p, args[6])

                            if len(args) > 7:
                                fics = args[7:]

                            else:
                                recebe_fic()

                            signal.signal(signal.SIGALRM, imprime_estado)
                            signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                            start_time = datetime.datetime.now()

                            p = []
                            for n in range(num_processes):
                                p.append(Process(target=pesquisa_fic, args=(1,)))
                            for proc in p:
                                proc.start()
                            for proc in p:
                                proc.join()

                            tempo_execucao = datetime.datetime.now() - start_time

                            binario = open(nome_fic.value, "ab")

                            pickle.dump(str(start_time), binario, 2)
                            pickle.dump(str(tempo_execucao), binario, 2)

                            binario.close()

                        else:
                            print
                            print "  Comando incorreto"
                            print "  Em caso de duvida, digite a opcao 'man'."
                            print
                            sys.exit()

                    else:

                        palavra = palavra = Value(ctypes.c_char_p, args[4])

                        if len(args) > 5:
                            fics = args[5:]

                        else:
                            recebe_fic()

                        signal.signal(signal.SIGALRM, imprime_estado)
                        signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                        start_time = datetime.datetime.now()

                        p = []
                        for n in range(num_processes):
                            p.append(Process(target=pesquisa_fic, args=(0,)))
                        for proc in p:
                            proc.start()
                        for proc in p:
                            proc.join()

                else:
                    print
                    print "  Comando incorreto"
                    print "  Em caso de duvida, digite a opcao 'man'."
                    print
                    sys.exit()


        ################################################### caso -p -f #

            elif args[2] == "-f":

                if len(args) >= 5:

                    nome_fic = Value(ctypes.c_char_p, args[3])

                    if os.path.isfile(nome_fic.value):
                        print
                        print "  O ficheiro " + nome_fic.value + " já existe."
                        print
                        sys.exit()

                    ##############################################################
                    ### FALTA try/except (o nome_fic não pode começar com "/") ###
                    ##############################################################
                            
                    ############################################ caso -p -f -a #

                    if args[4] == "-a":

                        if len(args) >= 7:

                            try:
                                intervalo_tempo = float(args[5])
                            except ValueError:
                                print
                                print "  Intervalo de tempo invalido."
                                print "  Em caso de duvida, digite a opcao 'man'."
                                print
                                sys.exit()

                            palavra = Value(ctypes.c_char_p, args[6])

                            if len(args) > 7:
                                fics = args[7:]

                            else:
                                recebe_fic()

                            signal.signal(signal.SIGALRM, imprime_estado)
                            signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                            start_time = datetime.datetime.now()

                            p = []
                            for n in range(num_processes):
                                p.append(Process(target=pesquisa_fic, args=(1,)))
                            for proc in p:
                                proc.start()
                            for proc in p:
                                proc.join()

                            tempo_execucao = datetime.datetime.now() - start_time

                            binario = open(nome_fic.value, "ab")

                            pickle.dump(str(start_time), binario, 2)
                            pickle.dump(str(tempo_execucao), binario, 2)

                            binario.close()

                        else:
                            print
                            print "  Comando incorreto"
                            print "  Em caso de duvida, digite a opcao 'man'."
                            print
                            sys.exit()

                    else:

                        palavra = Value(ctypes.c_char_p, args[4])

                        start_time = datetime.datetime.now()

                        if len(args) > 5:
                            fics = args[5:]

                        else:
                            recebe_fic()

                        p = []
                        for n in range(num_processes):
                            p.append(Process(target=pesquisa_fic, args=(1,)))
                        for proc in p:
                            proc.start()
                        for proc in p:
                            proc.join()

                        tempo_execucao = datetime.datetime.now() - start_time

                        binario = open(nome_fic.value, "ab")

                        pickle.dump(str(start_time), binario, 2)
                        pickle.dump(str(tempo_execucao), binario, 2)

                        binario.close()

                else:
                    print
                    print "  Comando incorreto"
                    print "  Em caso de duvida, digite a opcao 'man'."
                    print
                    sys.exit()


            else:
                palavra = Value(ctypes.c_char_p, args[2])

                if len(args) > 3:
                    fics = args[3:]

                else:
                    recebe_fic()

                p = []
                for n in range(num_processes):
                    p.append(Process(target=pesquisa_fic, args=(0,)))
                for proc in p:
                    proc.start()
                for proc in p:
                    proc.join()

        else:
            print
            print "  Comando incorreto"
            print "  Em caso de duvida, digite a opcao 'man'."
            print
            sys.exit()

############################################################## caso -a #

    elif args[0] == "-a":
        if len(args) >= 3:
            try:
                intervalo_tempo = float(args[1])
            except ValueError:
                print
                print "  Intervalo de tempo invalido."
                print "  Em caso de duvida, digite a opcao 'man'."
                print
                sys.exit()

        ################################################### caso -a -p #

            if args[2] == "-p":

                if len(args) >= 5:

                    try:
                        num_processes = int(float(args[3]))
                    except ValueError:
                        print
                        print "  Numero de processos invalido."
                        print "  Em caso de duvida, digite a opcao 'man'."
                        print
                        sys.exit()

                ############################################ caso -a -p -f #

                    if args[4] == "-f":

                        if len(args) >= 7:

                            nome_fic = Value(ctypes.c_char_p, args[5])

                            if os.path.isfile(nome_fic.value):
                                print
                                print "  O ficheiro " + nome_fic.value + " já existe."
                                print
                                sys.exit()

                            ##############################################################
                            ### FALTA try/except (o nome_fic não pode começar com "/") ###
                            ##############################################################
                            
                            palavra = Value(ctypes.c_char_p, args[6])

                            if len(args) > 7:
                                fics = args[7:]

                            else:
                                recebe_fic()

                            signal.signal(signal.SIGALRM, imprime_estado)
                            signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                            start_time = datetime.datetime.now()

                            p = []
                            for n in range(num_processes):
                                p.append(Process(target=pesquisa_fic, args=(1,)))
                            for proc in p:
                                proc.start()
                            for proc in p:
                                proc.join()

                            tempo_execucao = datetime.datetime.now() - start_time

                            binario = open(nome_fic.value, "ab")

                            pickle.dump(str(start_time), binario, 2)
                            pickle.dump(str(tempo_execucao), binario, 2)

                            binario.close()

                        else:
                            print
                            print "  Comando incorreto"
                            print "  Em caso de duvida, digite a opcao 'man'."
                            print
                            sys.exit()


                    else:

                        palavra = palavra = Value(ctypes.c_char_p, args[4])

                        if len(args) > 5:
                            fics = args[5:]

                        else:
                            recebe_fic()

                        signal.signal(signal.SIGALRM, imprime_estado)
                        signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                        start_time = datetime.datetime.now()

                        p = []
                        for n in range(num_processes):
                            p.append(Process(target=pesquisa_fic, args=(0,)))
                        for proc in p:
                            proc.start()
                        for proc in p:
                            proc.join()

                else:
                    print
                    print "  Comando incorreto"
                    print "  Em caso de duvida, digite a opcao 'man'."
                    print
                    sys.exit()


        ################################################### caso -a -f #

            elif args[2] == "-f":

                if len(args) >= 5:

                    nome_fic = Value(ctypes.c_char_p, args[3])

                    if os.path.isfile(nome_fic.value):
                        print
                        print "  O ficheiro " + nome_fic.value + " já existe."
                        print
                        sys.exit()

                    ##############################################################
                    ### FALTA try/except (o nome_fic não pode começar com "/") ###
                    ##############################################################
                            
                ############################################ caso -a -f -p #

                    if args[4] == "-p":

                        if len(args) >= 7:

                            try:
                                num_processes = int(float(args[5]))
                            except ValueError:
                                print
                                print "  Numero de processos invalido."
                                print "  Em caso de duvida, digite a opcao 'man'."
                                print
                                sys.exit()

                            palavra = Value(ctypes.c_char_p, args[6])

                            if len(args) > 7:
                                fics = args[7:]

                            else:
                                recebe_fic()

                            signal.signal(signal.SIGALRM, imprime_estado)
                            signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                            start_time = datetime.datetime.now()

                            p = []
                            for n in range(num_processes):
                                p.append(Process(target=pesquisa_fic, args=(1,)))
                            for proc in p:
                                proc.start()
                            for proc in p:
                                proc.join()

                            tempo_execucao = datetime.datetime.now() - start_time

                            binario = open(nome_fic.value, "ab")

                            pickle.dump(str(start_time), binario, 2)
                            pickle.dump(str(tempo_execucao), binario, 2)

                            binario.close()

                        else:
                            print
                            print "  Comando incorreto"
                            print "  Em caso de duvida, digite a opcao 'man'."
                            print
                            sys.exit()


                    else:

                        palavra = palavra = Value(ctypes.c_char_p, args[4])

                        if len(args) > 5:
                            fics = args[5:]

                        else:
                            recebe_fic()

                        signal.signal(signal.SIGALRM, imprime_estado)
                        signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                        start_time = datetime.datetime.now()

                        pesquisa_fic(1)

                        tempo_execucao = datetime.datetime.now() - start_time

                        binario = open(nome_fic.value, "ab")

                        pickle.dump(str(start_time), binario, 2)
                        pickle.dump(str(tempo_execucao), binario, 2)

                        binario.close()

                else:
                    print
                    print "  Comando incorreto"
                    print "  Em caso de duvida, digite a opcao 'man'."
                    print
                    sys.exit()


            else:
                palavra = palavra = Value(ctypes.c_char_p, args[2])

                if len(args) > 3:
                    fics = args[3:]

                else:
                    recebe_fic()

                signal.signal(signal.SIGALRM, imprime_estado)
                signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                start_time = datetime.datetime.now()

                pesquisa_fic(0)

        else:
            print
            print "  Comando incorreto"
            print "  Em caso de duvida, digite a opcao 'man'."
            print
            sys.exit()

############################################################## caso -f #

    elif args[0] == "-f":

        if len(args) >= 3:

            nome_fic = Value(ctypes.c_char_p, args[1])

            ##############################################################
            ### FALTA try/except (o nome_fic não pode começar com "/") ###
            ##############################################################
                            
            if os.path.isfile(nome_fic.value):
                print
                print "  O ficheiro " + nome_fic.value + " já existe."
                print
                sys.exit()

        ################################################### caso -f -p #

            if args[2] == "-p":

                if len(args) >= 5:

                    try:
                        num_processes = int(float(args[3]))
                    except ValueError:
                        print
                        print "  Numero de processos invalido."
                        print "  Em caso de duvida, digite a opcao 'man'."
                        print
                        sys.exit()


                ############################################ caso -f -p -a #

                    if args[4] == "-a":

                        if len(args) >= 7:

                            try:
                                intervalo_tempo = float(args[5])
                            except ValueError:
                                print
                                print "  Intervalo de tempo invalido."
                                print "  Em caso de duvida, digite a opcao 'man'."
                                print
                                sys.exit()

                            palavra = Value(ctypes.c_char_p, args[6])

                            if len(args) > 7:
                                fics = args[7:]

                            else:
                                recebe_fic()

                            signal.signal(signal.SIGALRM, imprime_estado)
                            signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                            start_time = datetime.datetime.now()

                            p = []
                            for n in range(num_processes):
                                p.append(Process(target=pesquisa_fic, args=(1,)))
                            for proc in p:
                                proc.start()
                            for proc in p:
                                proc.join()

                            tempo_execucao = datetime.datetime.now() - start_time

                            binario = open(nome_fic.value, "ab")

                            pickle.dump(str(start_time), binario, 2)
                            pickle.dump(str(tempo_execucao), binario, 2)

                            binario.close()

                        else:
                            print
                            print "  Comando incorreto"
                            print "  Em caso de duvida, digite a opcao 'man'."
                            print
                            sys.exit()


                    else:

                        palavra = Value(ctypes.c_char_p, args[4])

                        start_time = datetime.datetime.now()

                        if len(args) > 5:
                            fics = args[5:]

                        else:
                            recebe_fic()

                        p = []
                        for n in range(num_processes):
                            p.append(Process(target=pesquisa_fic, args=(1,)))
                        for proc in p:
                            proc.start()
                        for proc in p:
                            proc.join()

                        tempo_execucao = datetime.datetime.now() - start_time

                        binario = open(nome_fic.value, "ab")

                        pickle.dump(str(start_time), binario, 2)
                        pickle.dump(str(tempo_execucao), binario, 2)

                        binario.close()

                else:
                    print
                    print "  Comando incorreto"
                    print "  Em caso de duvida, digite a opcao 'man'."
                    print
                    sys.exit()


        ################################################### caso -f -a #

            elif args[2] == "-a":

                if len(args) >= 5:

                    try:
                        intervalo_tempo = float(args[3])
                    except ValueError:
                        print
                        print "  Intervalo de tempo invalido."
                        print "  Em caso de duvida, digite a opcao 'man'."
                        print
                        sys.exit()

                ############################################ caso -f -a -p #

                    if args[4] == "-p":

                        if len(args) >= 7:

                            try:
                                num_processes = int(float(args[5]))
                            except ValueError:
                                print
                                print "  Numero de processos invalido."
                                print "  Em caso de duvida, digite a opcao 'man'."
                                print
                                sys.exit()

                            palavra = Value(ctypes.c_char_p, args[6])

                            if len(args) > 7:
                                fics = args[7:]

                            else:
                                recebe_fic()

                            signal.signal(signal.SIGALRM, imprime_estado)
                            signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                            start_time = datetime.datetime.now()

                            p = []
                            for n in range(num_processes):
                                p.append(Process(target=pesquisa_fic, args=(1,)))
                            for proc in p:
                                proc.start()
                            for proc in p:
                                proc.join()

                            tempo_execucao = datetime.datetime.now() - start_time

                            binario = open(nome_fic.value, "ab")

                            pickle.dump(str(start_time), binario, 2)
                            pickle.dump(str(tempo_execucao), binario, 2)

                            binario.close()

                        else:
                            print
                            print "  Comando incorreto"
                            print "  Em caso de duvida, digite a opcao 'man'."
                            print
                            sys.exit()


                    else:

                        palavra = palavra = Value(ctypes.c_char_p, args[4])

                        if len(args) > 5:
                            fics = args[5:]

                        else:
                            recebe_fic()

                        signal.signal(signal.SIGALRM, imprime_estado)
                        signal.setitimer(signal.ITIMER_REAL, intervalo_tempo, intervalo_tempo)

                        start_time = datetime.datetime.now()

                        pesquisa_fic(1)

                        tempo_execucao = datetime.datetime.now() - start_time

                        binario = open(nome_fic.value, "ab")

                        pickle.dump(str(start_time), binario, 2)
                        pickle.dump(str(tempo_execucao), binario, 2)

                        binario.close()

                else:
                    print
                    print "  Comando incorreto"
                    print "  Em caso de duvida, digite a opcao 'man'."
                    print
                    sys.exit()


            else:
                palavra = palavra = Value(ctypes.c_char_p, args[2])

                if len(args) > 3:
                    fics = args[3:]

                else:
                    recebe_fic()

                start_time = datetime.datetime.now()

                pesquisa_fic(1)

                tempo_execucao = datetime.datetime.now() - start_time

                binario = open(nome_fic.value, "ab")

                pickle.dump(str(start_time), binario, 2)
                pickle.dump(str(tempo_execucao), binario, 2)

                binario.close()

        else:
            print
            print "  Comando incorreto"
            print "  Em caso de duvida, digite a opcao 'man'."
            print
            sys.exit()

############################################################## manual #

    elif args[0] == "man":
        print
        print "  \t\t\tMANUAL:"
        print
        print "  SINOPSE:"
        print
        print "  \tpgrep [-p n] [-a s] [-f file] palavra {ficheiros}"
        print
        print
        print "  DESCRIÇÃO"
        print
        print "  \tConta o número de linhas, nos ficheiros dados, em que a palavra apareceu"
        print
        print "  \t[-p n]:"
        print "  \t\tÉ opcional. Corre o programa com 'n' processos"
        print
        print "  \t[-a s]:"
        print "  \t\tÉ opcional. De 's' em 's' segundos, dá o estado da pesquisa"
        print
        print "  \t[-f file]:"
        print "  \t\tÉ opcional. Cria um ficheiro binário com o nome 'file', onde guarda o histórico da execução do programa."
        print "  \t\tEste histórico pode ser lido usando o programa lgrep.py"
        print
        print "  \tpalavra:"
        print "  \t\tÉ obrigatória. Indica a palavra para procurar"
        print
        print "  \t{ficheiros}:"
        print "  \t\tE obrigatório. Pode ser um ou mais ficheiros, onde vai ser efetuada a procura."
        print "  \t\tCaso não sejam dados nenhuns ficheiros neste campo, entrara num ciclo que lhe vai pedir ficheiros até que saia."
        print
        sys.exit()

############################################################## sem casos #

    else:
        palavra = palavra = Value(ctypes.c_char_p, args[0])

        if len(args) > 1:
            fics = args[1:]

        else:
            recebe_fic()

        pesquisa_fic(0)

print
print "  A palavra '" + palavra.value + "' foi encontrada em", num_linhas.value, "linhas"
print